const express = require('express')
const morgan = require('morgan')
const favicon = require('serve-favicon')
//permette de récupérer des données à la base en chaine de caracèteres pour les passer en JSON
const bodyParser = require('body-parser')
const sequelize = require('./src/db/sequelize')

//démarrage du serveur express
const app = express()
const port = 3000

app
    .use(favicon(__dirname + '/favicon.ico'))
    .use(morgan('dev'))
    .use(bodyParser.json())

sequelize.initDb()

//ici intégration des différents points de terminaison CRUD
require('./src/routes/findAllPokemons')(app)
require('./src/routes/findOnePokemonByPk')(app)
require('./src/routes/createPokemon')(app)
require('./src/routes/upDatePokemon')(app)
require('./src/routes/deletePokemon')(app)

//gestion des erreurs 404
app.use(({ res }) => {
    const message = 'Impossible de trouver la ressource demandée. Essayez une nouvelle URL.'
    res.status(404).json({ message })
})



app.listen(port, () => console.log(`Notre application Node est démarrée sur : http://localhost:${port}`))
