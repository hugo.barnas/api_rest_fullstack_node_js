
const validTypes = ['Plante', 'Poison', 'Feu', 'Eau', 'Insecte', 'Vol', 'Normal', 'Electrik', 'Fée']


module.exports = (sequelize, DataTypes) => {
    return sequelize.define('Pokemon', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING,
            allowNull: false,
            unique: {
                msg: 'Ce nom est déjà pris'
            },
            validate: {
                notEmpty: { msg: 'Le nom du pokemon ne doit pas être vide.' },
                notNull: { msg: 'Le nom est une propriété requise' }
            }

        },
        hp: {
            type: DataTypes.INTEGER,
            allowNull: false,
            validate: {
                isInt: { msg: 'Utiliser uniquement des nombres entiers pour des points de vie.' },
                notNull: { msg: 'Les points de vie sont une propriété requise' },
                min: {
                    args: [0],
                    msg: 'Les points de vie sont supérieurs ou égales à 0'
                },
                max: {
                    args: [999],
                    msg: 'Les points de vie ne peut pas être supérieur à 999'
                }
            }
        },
        cp: {
            type: DataTypes.INTEGER,
            allowNull: false,
            validate: {
                isInt: { msg: 'Utiliser uniquement des nombres entiers pour les points de dégâts.' },
                notNull: { msg: 'Les points de dégats sont une propriété requise' },
                min: {
                    args: [0],
                    msg: 'Les points de vie sont égales ou supérieur à 0'
                },
                max: {
                    args: [99],
                    msg: 'Les points de vie ne peut pas être supérieur à 999'
                }
            }
        },
        picture: {
            type: DataTypes.STRING,
            allowNull: false,
            validate: {
                isUrl: { msg: 'Vous devez insérer une url valide pour l\'image' },
                notNull: { msg: 'L\'image est une propriété requise' }
            }
        },
        types: {
            type: DataTypes.STRING,
            allowNull: false,
            get() {
                return this.getDataValue('types').split(',')
            },
            set(types) {
                this.setDataValue('types', types.join())
            },
            validate: {
                isTypesValid(value) {
                    if (!value) {
                        throw new Error('Un pokemon doit au moins avoir un type.')
                    }
                    if (value.split(',').length > 3) {
                        throw new Error('Un type ne peut pas avoir plus de trois types.')
                    }
                    value.split(',').forEach(type => {
                        if (!validTypes.includes(type)) {
                            throw new Error(`Un type doit appartenir à la liste suivante : ${validTypes}`);
                        }

                    });
                }
            }
        }
    }, {
        timestamps: true,
        createdAt: 'created',
        updatedAt: false
    })
}