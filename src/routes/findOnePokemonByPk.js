const pokemons = require('../db/mock-pokemon');
const { Pokemon } = require('../db/sequelize');

module.exports = (app) => {
    app.get('/api/pokemons/:id', (req, res) => {
        Pokemon.findByPk(req.params.id)
            .then(pokemon => {
                if (pokemon === null) {
                    const message = `Le pokemon n'existe pas`
                    res.json({ message, data: pokemon })
                }
                const message = 'Un pokemon a été trouvé'
                res.json({ message, data: pokemon })
            })
            .catch(error => {
                const message = 'Le pokemon recherché n\a pas été trouvé, merci de renouveller votre demande.'
                res.statut(500).json({ message, data: error })
            })
    })
}

