const { ValidationError, UniqueConstraintError } = require('sequelize/dist');
const { Pokemon } = require('../db/sequelize');

module.exports = (app) => {
    app.put('/api/pokemons/:id', (req, res) => {
        const id = req.params.id
        Pokemon.update(req.body, {
            where: { id: id }
        })
            .then(_ => {
                //récupérer le pokemon et le retourner au client
                return Pokemon.findByPk(id).then(pokemon => {
                    if (pokemon === null) {
                        const message = `Le pokemon demandé n'existe pas. Rééssayez avec un autre identifiant`
                        return res.status(400).json({ message })
                    }
                    const message = `Le pokemon ${pokemon.name} a bien été modifié.`
                    res.json({ message, data: pokemon })
                })
            })
            .catch(error => {
                if (error instanceof ValidationError) {
                    return res.status(400).json({ message: error.message, data: error })
                }
                if (error instanceof UniqueConstraintError) {
                    return res.status(400).json({ message: error.message, data: error })
                }
                const message = `Le pokemon n'a pas pu être modifié, rééssayez qq instants.`
                res.status(500).json({ message, data: error })
            })

    })
}