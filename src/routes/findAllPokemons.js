const { Pokemon } = require('../db/sequelize')

module.exports = (app) => {
    app.get('/api/pokemons', (req, res) => {
        if (req.query.name) {
            const name = req.query.name
            return Pokemon.findAll({ where: { name: name } })
                .then(pokemons => {
                    const message = `Il y a ${pokemons.length} pokemons qui correspond au terme de recherche ${name}.`
                    res.json({ message, data: pokemons })
                })
        }
        Pokemon.findAll()
            .then(pokemons => {
                const message = 'La liste des pokemons a bien été récupérée'
                res.json({ message, data: pokemons })
            })
            .catch((error => {
                const message = `La liste de Pokemon n'a pas pu être récupérée, merci d'attendre quelques instants.`
                res.statut(500).json({ message, data: error })
            }))
    })
}