/*
  Warnings:

  - Added the required column `cp` to the `Pokemon` table without a default value. This is not possible if the table is not empty.
  - Added the required column `hp` to the `Pokemon` table without a default value. This is not possible if the table is not empty.
  - Added the required column `type` to the `Pokemon` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE `Pokemon` ADD COLUMN `cp` INTEGER NOT NULL,
    ADD COLUMN `hp` INTEGER NOT NULL,
    ADD COLUMN `type` VARCHAR(255) NOT NULL;
